import java.util.Scanner;

public class Main {

   public static String reverseInt(int num){
        String str = Integer.toString(num);
        String rev= "";

        for(int i = str.length() -1; i >=0 ; --i){
            rev =  rev + str.charAt(i);
        }

       return rev;
   }


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter an integer: ");
        int num = sc.nextInt();
        System.out.println(reverseInt(num));
    }
}
